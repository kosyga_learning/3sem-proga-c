#pragma once
#include <iostream>
int compare(const void* x1, const void* x2)   // ������� ��������� ��������� �������
{
	return (*(int*)x1 - *(int*)x2);              // ���� ��������� ��������� ����� 0, �� ����� �����, < 0: x1 < x2; > 0: x1 > x2
}
namespace Lab4 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form5
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^ textBox1;
	protected:
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::RichTextBox^ answers;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Button^ button2;
	private: double* array;
	private: int n;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Label^ label2;
		   //private: System::Windows::Forms::TextBox^ textBoxes[2];
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::TextBox^ textBox4;
	private: System::Windows::Forms::TextBox^ textBox5;
	private: System::Windows::Forms::TextBox^ textBox6;
	private: System::Windows::Forms::TextBox^ textBox7;
	private: System::Windows::Forms::TextBox^ textBox8;
	private: System::Windows::Forms::TextBox^ textBox9;
	private: System::Windows::Forms::TextBox^ textBox10;
	private: System::Windows::Forms::TextBox^ textBox11;
	private: System::Windows::Forms::TextBox^ textBox12;
	private: System::Windows::Forms::TextBox^ textBox13;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Label^ label3;







	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->answers = (gcnew System::Windows::Forms::RichTextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->textBox13 = (gcnew System::Windows::Forms::TextBox());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(40, 34);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 0;
			this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox1_KeyPress);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(15, 87);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(52, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"�����";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::Summ);
			// 
			// answers
			// 
			this->answers->Location = System::Drawing::Point(40, 145);
			this->answers->Name = L"answers";
			this->answers->Size = System::Drawing::Size(100, 96);
			this->answers->TabIndex = 2;
			this->answers->Text = L"";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 18);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(221, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"������� �������� ������� ����� ������";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(73, 87);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 4;
			this->button2->Text = L"���-�� = 0";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::null);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(51, 58);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 5;
			this->button3->Text = L"�����";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::next);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(15, 117);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(63, 23);
			this->button4->TabIndex = 6;
			this->button4->Text = L"max/min";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(378, 18);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(88, 13);
			this->label2->TabIndex = 7;
			this->label2->Text = L"������ �������";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(328, 45);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(49, 20);
			this->textBox2->TabIndex = 8;
			this->textBox2->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox2_KeyPress);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(383, 45);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(49, 20);
			this->textBox3->TabIndex = 9;
			this->textBox3->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox3_KeyPress);
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(438, 45);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(49, 20);
			this->textBox4->TabIndex = 10;
			this->textBox4->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox4_KeyPress);
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(328, 71);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(49, 20);
			this->textBox5->TabIndex = 13;
			this->textBox5->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox5_KeyPress);
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(383, 71);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(49, 20);
			this->textBox6->TabIndex = 12;
			this->textBox6->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox6_KeyPress);
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(438, 71);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(49, 20);
			this->textBox7->TabIndex = 11;
			this->textBox7->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox7_KeyPress);
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(328, 97);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(49, 20);
			this->textBox8->TabIndex = 19;
			this->textBox8->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox8_KeyPress);
			// 
			// textBox9
			// 
			this->textBox9->Location = System::Drawing::Point(383, 97);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(49, 20);
			this->textBox9->TabIndex = 18;
			this->textBox9->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox9_KeyPress);
			// 
			// textBox10
			// 
			this->textBox10->Location = System::Drawing::Point(438, 97);
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(49, 20);
			this->textBox10->TabIndex = 17;
			this->textBox10->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox10_KeyPress);
			// 
			// textBox11
			// 
			this->textBox11->Location = System::Drawing::Point(328, 123);
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(49, 20);
			this->textBox11->TabIndex = 16;
			this->textBox11->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox11_KeyPress);
			// 
			// textBox12
			// 
			this->textBox12->Location = System::Drawing::Point(383, 123);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(49, 20);
			this->textBox12->TabIndex = 15;
			this->textBox12->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox12_KeyPress);
			// 
			// textBox13
			// 
			this->textBox13->Location = System::Drawing::Point(438, 123);
			this->textBox13->Name = L"textBox13";
			this->textBox13->Size = System::Drawing::Size(49, 20);
			this->textBox13->TabIndex = 14;
			this->textBox13->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::textBox13_KeyPress);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(349, 163);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(117, 23);
			this->button5->TabIndex = 20;
			this->button5->Text = L"����� � ��� ����";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(394, 207);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(0, 13);
			this->label3->TabIndex = 21;
			// 
			// Form5
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(583, 261);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->textBox8);
			this->Controls->Add(this->textBox9);
			this->Controls->Add(this->textBox10);
			this->Controls->Add(this->textBox11);
			this->Controls->Add(this->textBox12);
			this->Controls->Add(this->textBox13);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->textBox7);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->answers);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->Name = L"Form5";
			this->Text = L"Form5";
			this->Load += gcnew System::EventHandler(this, &Form1::Form5_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Summ(System::Object^ sender, System::EventArgs^ e)
	{
		double s = 0;
		for (int i = 0; i < n; i++)
			if ((array[i] >= 0) && (i % 2 == 0))
				s += array[i];
		answers->Text = "����� ������������� ��������� c ������� �������� = " + Convert::ToString(s);
	}
	private: System::Void null(System::Object^ sender, System::EventArgs^ e)
	{
		int k = 0;
		for (int i = 0; i < n; i++)
			if (array[i] == 0)
				k++;
		answers->Text = "���-�� ������� ��������� = " + Convert::ToString(k);
	}
	private: System::Void textBox1_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((textBox1->TextLength == 0) && (isspace(e->KeyChar))) e->KeyChar = 0;
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (!isspace(e->KeyChar) && (e->KeyChar != '\b'))) e->KeyChar = 0;
		if (textBox1->TextLength != 0)
		{
			if ((textBox1->Text[textBox1->TextLength - 1] == '-') && ((e->KeyChar == '.') || (isspace(e->KeyChar)) || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox1->Text[textBox1->TextLength - 1])) && ((e->KeyChar == '.') || (isspace(e->KeyChar)))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox1->Text[textBox1->TextLength - 1])) && (e->KeyChar == '-')) e->KeyChar = 0;
		}
	}
	private: System::Void next(System::Object^ sender, System::EventArgs^ e)
	{
		String^ t;
		textBox1->Text += " ";
		int k = 0;
		for (int i = 0; Convert::ToDouble(textBox1->TextLength) > i; i++)
			if (isspace(textBox1->Text[i]))
				n += 1;
		array = new double[n];
		for (int i = 0; Convert::ToDouble(textBox1->TextLength) > i; i++)
		{
			if (!(isspace(textBox1->Text[i])))
				t += textBox1->Text[i];
			else
			{
				array[k] = Convert::ToDouble(t);
				t = "";
				k++;
			}
		}
		textBox1->Hide();
		button3->Hide();
		button1->Show();
		button2->Show();
		button4->Show();
	}
	private: System::Void Form5_Load(System::Object^ sender, System::EventArgs^ e)
	{
		button1->Hide();
		button2->Hide();
		button4->Hide();
	}

	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e)
	{
		double min = 100000;
		double max = -100000;
		for(int i = 0;i < n; i++)
		{
			if (array[i] > max) max = array[i];
			if (array[i] < min) min = array[i];
		}
		if (array[0] != 0)
			answers->Text = "Max/Min = " + Convert::ToString(max / min);
		else
			answers->Text = "Error: Min = 0";
	}
	private: System::Void textBox2_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox2->TextLength != 0)
		{
			if ((textBox2->Text[textBox2->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox2->Text[textBox2->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox2->Text[textBox2->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox3_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox3->TextLength != 0)
		{
			if ((textBox3->Text[textBox3->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox3->Text[textBox3->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox3->Text[textBox3->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox4_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox4->TextLength != 0)
		{
			if ((textBox4->Text[textBox4->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox4->Text[textBox4->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox4->Text[textBox4->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox7_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox7->TextLength != 0)
		{
			if ((textBox7->Text[textBox7->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox7->Text[textBox7->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox7->Text[textBox7->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox6_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox6->TextLength != 0)
		{
			if ((textBox6->Text[textBox6->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox6->Text[textBox6->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox6->Text[textBox6->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox5_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox5->TextLength != 0)
		{
			if ((textBox5->Text[textBox5->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox5->Text[textBox5->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox5->Text[textBox5->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox13_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox13->TextLength != 0)
		{
			if ((textBox13->Text[textBox13->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox13->Text[textBox13->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox13->Text[textBox13->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox12_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox12->TextLength != 0)
		{
			if ((textBox12->Text[textBox12->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox12->Text[textBox12->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox12->Text[textBox12->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox11_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox11->TextLength != 0)
		{
			if ((textBox11->Text[textBox11->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox11->Text[textBox11->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox11->Text[textBox11->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox10_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox10->TextLength != 0)
		{
			if ((textBox10->Text[textBox10->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox10->Text[textBox10->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox10->Text[textBox10->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox9_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox9->TextLength != 0)
		{
			if ((textBox9->Text[textBox9->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox9->Text[textBox9->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox9->Text[textBox9->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox8_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox8->TextLength != 0)
		{
			if ((textBox8->Text[textBox8->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox8->Text[textBox8->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox8->Text[textBox8->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e)
	{
		double summSbs[3];
		double minsumm = 9999999999999;
		int min;
		summSbs[0] = Convert::ToDouble(textBox2->Text) + Convert::ToDouble(textBox5->Text) + Convert::ToDouble(textBox8->Text) + Convert::ToDouble(textBox11->Text);
		summSbs[1] = Convert::ToDouble(textBox3->Text) + Convert::ToDouble(textBox6->Text) + Convert::ToDouble(textBox9->Text) + Convert::ToDouble(textBox12->Text);
		summSbs[2] = Convert::ToDouble(textBox4->Text) + Convert::ToDouble(textBox7->Text) + Convert::ToDouble(textBox10->Text) + Convert::ToDouble(textBox13->Text);
		for (int i = 0; i < 3; i++)
			if (summSbs[i] < minsumm)
			{
				minsumm = summSbs[i];
				min = i;
			}
		label3->Text = Convert::ToString(min + 1);
	}
	};
}
