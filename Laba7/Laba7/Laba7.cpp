﻿#include <iostream>
int min(int *one, int *two, int *min)
{
    int s = (*one)+(*two);
    int m = *(min);
    if (s < m) return s;
    else return (*min);
}
void out(int** array, int N, int M)
{
    //вывожу массив
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
            std::cout << array[i][j] << " ";
        std::cout << "\n";
    }
}
void sum(int** array, int N, int M)
{
    std::cout << "Суммы со строками не содержащими <0: " << "\n";
    std::string text;
    for (int i = 0; i < N; i++)
    {
        int s = 0;
        bool t = true;
        for (int j = 0; j < M; j++)
        {
            if (array[i][j] < 0) t = false;
            s += array[i][j];
        }
        if (t)std::cout << s << ' ' << '\n';
    }
}
void minnoone(int** array, int N, int M)
{
    int min = 99999;
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            if (array[i][j] < min) 
            {
                int k = 0;
                for (int n = 0; n < N; n++)
                    for (int m = 0; m < M; m++)
                        if (array[n][m] == array[i][j]) k++;
                if (k > 1) min = array[i][j];
            }
    std::cout << "Мин число, кот. встр. > 1 раза = " << min << "\n";
}
void ch1()
{
    int N;
    int minimum = 999999;
    std::cout << "Введите размер массива" << '\n';
    std::cin >> N;
    int* ar = new int[N];
    for (int i = 0; i < N; i++)
    {
        std::cout << "Введите " << i + 1 << " элемент массива" << '\n';
        std::cin >> ar[i];
    }
    for (int i = 1; i < N; i++)
        minimum = min(&ar[i - 1], &ar[i], &minimum);
    std::cout << minimum;
}
void ch2()
{
    // Объявляю этот сраный динамический двумерный массив
    int N, M;
    std::cout << "Введите размер массива" << '\n';
    std::cin >> N >> M;
    int* ar = new int[M];
    int** array = new int* [N];
    for (int i = 0; i < N; i++)
    {
        array[i] = new int[M];
        for (int j = 0; j < M; j++)//инициализирую его
        {
            std::cout << "Введите " << j << " элемент " << i << " строки" << '\n';
            std::cin >> array[i][j];
        }
    }
    out(array, N, M);
    sum(array, N, M);
    minnoone(array, N, M);
}
int main()
{
    int n;
    setlocale(LC_ALL, "rus");
    std::cin >> n;
    switch(n)
    {
    case 1:ch1();
        break;
    case 2:ch2();
        break;
    }
}
