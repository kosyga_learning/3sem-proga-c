#include <string>
#include <iostream>
#include <vector>
#include <cstring>
#include <fstream>
#include <windows.h>
#include <iomanip>

void task1()
{
    std::string fileName;
    std::string line;
    std::string oldText;
    std::string word;
    int k = 0;
    int l = 0;
    int j = 0;
    std::cout << "������� �������� �����" << '\n';
    std::cin >> fileName;

    std::ifstream in(fileName + ".txt");
    if (in.is_open())
    {
        while (getline(in, line))
        {
            std::cout << line << '\n';
            oldText += line;
        }
    }
    in.close();
    std::cout << '\n';
    for (int i = 0; i < oldText.length(); i++) if ((oldText[i] == '.') || (oldText[i] == '!') || (oldText[i] == '?')) k++;
    std::cout << k << '\n';
    std::string* text = new std::string[k];
    for (int i = 0, t = 0; i < oldText.length() - 1; i++) {
        if ((oldText[i] == '.') || (oldText[i] == '!') || (oldText[i] == '?'))
        {
            text[t] += oldText[i];
            t++;
        }
        else text[t] += oldText[i];
    }
    std::cout << "������� �����" << '\n';
    std::cin >> word;
    for (int i = 0; i < k; i++) if (text[i].find(word)) l++;
    if (l == 0) std::cout << "�� ������� �� ������ ����� � ���� ������" << '\n';
    else {
        std::ofstream out("newfile.txt", std::ios::app);
        if (out.is_open()) for (int i = 0; i < k; i++) if (text[i].find(word) != std::string::npos) out << text[i] << '\n';
        out.close();
    }
}

void task2()
{
	int UserAnswer = 0, TotalTickets;
	float PriceTicket;
	char filename[] = "railwaybase.dat";
	const int R = 40, TS = 20;
	char TrainRoute[R], Train[TS];

	int otvet = 0;
	bool end = false;
	while(!end)
	{
		std::cout << '\n' << "===================RailwayBase=====================" << '\n';
		std::cout << '\n' << "1 - �������� ������;  2 - ��� ������;" << '\n';
		std::cout << '\n' << "3 - ������� ������;  4 - ������;" << '\n';
		std::cout << '\n' << "===================================================" << '\n';
		std::cout << '\n' << "��� �����: ";
		std::cin >> otvet;

		std::ofstream out(filename, std::ios::app | std::ios::binary);
		std::ifstream fin(filename, std::ios::binary);
		switch (otvet)
		{
		case 1:
			if (!out) { std::cout << "File Error!"; return; }
			std::cin.get();
			std::cout << '\n' << "����� ������: "; std::cin.getline(TrainRoute, R);
			std::cout << '\n' << "����� �����������-��������: "; std::cin.getline(Train, TS);
			std::cout << '\n' << "���-�� ��������� �������: "; std::cin >> TotalTickets;
			std::cout << '\n' << "���� ������: "; std::cin >> PriceTicket;
			out.write(TrainRoute, R);
			out.write(Train, TS);
			out.write(reinterpret_cast<char*>(&TotalTickets), sizeof(int));
			out.write(reinterpret_cast<char*>(&PriceTicket), sizeof(float));
			break;
		case 2:
			if (!fin) { std::cout << "File Error!"; return; }
			while (!fin.eof()) {
				fin.read(TrainRoute, R);
				fin.read(Train, TS);
				fin.read(reinterpret_cast<char*>(&TotalTickets), sizeof(int));
				fin.read(reinterpret_cast<char*>(&PriceTicket), sizeof(float));
				if (fin.good())
				{
					std::cout << '\n' << std::setw(R) << TrainRoute << std::setw(TS) << Train
						<< std::setw(5) << TotalTickets << std::setw(7) << PriceTicket << '\n';
				}
				else break;
			}
			break;
		case 3:
			std::cout << '\n' << "����� ������: ";
			char n_pv[24];
			std::cin >> n_pv;
			if (!fin) { std::cout << "File Error!"; return; }
			{
				fin.read(TrainRoute, R);
				fin.read(Train, TS);
				fin.read(reinterpret_cast<char*>(&TotalTickets), sizeof(int));
				fin.read(reinterpret_cast<char*>(&PriceTicket), sizeof(float));
				if (strcmp(TrainRoute, n_pv) == 0) { std::cout << '\n' << "������� ������ �����: " << n_pv << ": " << TotalTickets * PriceTicket; }
			}
			break;
		case 4: end=true;
			break;
		default: std::cout << "�� ����� ����������� ��������" << '\n';
		}
		out.close();
		fin.close();
	}
}

int main() {
    setlocale(LC_ALL, "rus");
    int n;
	while(true)
	{
		std::cin >> n;
		switch (n)
		{
		case 1: task1();
			break;
		case 2: task2();
			break;
		}
	}
}