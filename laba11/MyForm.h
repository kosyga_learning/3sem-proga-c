#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <cstring>
#include <fstream>
#include <windows.h>
#include <iomanip>

namespace $safeprojectname$ {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^ pictureBox1;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button4;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(13, 13);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(391, 362);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::pictureBox1_MouseClick);
			this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::pictureBox1_MouseMove);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(497, 13);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"connect";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(497, 43);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 2;
			this->button2->Text = L"save";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(497, 73);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 3;
			this->button3->Text = L"clear";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(497, 103);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 4;
			this->button4->Text = L"load";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(853, 441);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	array <Point>^ points;
	Pen^ p;
	int id = 0;
	int mouseX;
	int mouseY;
	int k = 0;
	void clear()
	{
		id = 0;
		k = 0;
		Graphics^ g = pictureBox1->CreateGraphics();
		g->Clear(System::Drawing::Color::FromArgb(255, 255, 255));
		points->Resize(points,0);
		points = gcnew array <Point>(40);
	}
	private: System::Void pictureBox1_MouseClick(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e) 
	{
		Graphics^ g = pictureBox1->CreateGraphics();
		points[id].X = mouseX;
		points[id].Y = mouseY;
		id++;
		k++;
		g->FillEllipse(Brushes::Red,mouseX,mouseY,10,10);
	}
	private: System::Void MyForm_Load(System::Object^ sender, System::EventArgs^ e) 
	{
		points = gcnew array <Point>(40);
	}
	private: System::Void pictureBox1_MouseMove(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e) 
	{
		mouseX = e->Location.X;
		mouseY = e->Location.Y;
		p = gcnew Pen(System::Drawing::Color::FromArgb(0, 0, 255));
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Graphics^ g = pictureBox1->CreateGraphics();
		int i = 1;
		if(k>=3)
		{
			while ((points[i].X!=0)||(points[i].Y!=0))
			{
				g->DrawLine(p, points[i - 1], points[i]);
				i++;
			}
			g->DrawLine(p, points[0], points[i-1]);
		}
	}
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		std::ofstream out("newfile.txt", std::ios::app);
		int i = 0;
		while ((points[i].X != 0) || (points[i].Y != 0))
		{
			out << points[i].X << ' ' << points[i].Y << '\n';
			i++;
		}
	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		clear();
	}
	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		clear();
		Graphics^ g = pictureBox1->CreateGraphics();
		std::string line;
		
		std::ifstream in("newfile.txt");
		if (in.is_open())
		{
			int i = 0;
			while (getline(in, line))
			{
				String^ strx = "";
				String^ stry = "";
				for (int f = 0; f < line.length(); f++)
				{
					bool isx = true;
					if (line[f] == ' ') { isx = false; f++; }
					if (isx) strx += Convert::ToChar(line[f]);
					else stry += Convert::ToChar(line[f]);
				}
				points[i] = Point(Convert::ToInt16(strx), Convert::ToInt16(stry));
				g->FillEllipse(Brushes::Red, points[i].X, points[i].Y, 10, 10);
				k++;
				i++;
			}
			int h = 1;
			if (k >= 3)
			{
				while ((points[i].X != 0) || (points[i].Y != 0))
				{
					g->DrawLine(p, points[h - 1], points[h]);
					h++;
				}
				g->DrawLine(p, points[0], points[h - 1]);
			}
		}
	}
};
}
