#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <array>

namespace CppCLRWinFormsProject {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^ pictureBox1;
	protected:
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Button^ button6;
	private: System::Windows::Forms::Button^ button7;
	private: System::Windows::Forms::Button^ button8;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::ColorDialog^ colorDialog1;
	private: System::Windows::Forms::Button^ button9;
	private: System::Windows::Forms::Button^ button10;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(13, 13);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(391, 362);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::pictureBox1_MouseClick);
			this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::pictureBox1_MouseMove);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(497, 13);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"connect";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::conect);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(497, 43);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 2;
			this->button2->Text = L"save";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::save);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(497, 73);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 3;
			this->button3->Text = L"clear";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(497, 103);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 4;
			this->button4->Text = L"load";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::load);
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(497, 133);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(75, 23);
			this->button9->TabIndex = 5;
			this->button9->Text = L"P";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &Form1::button9_Click);
			// 
			// button10
			// 
			this->button10->Location = System::Drawing::Point(497, 163);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(75, 23);
			this->button10->TabIndex = 6;
			this->button10->Text = L"S";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &Form1::button10_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(853, 441);
			this->Controls->Add(this->button10);
			this->Controls->Add(this->button9);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"Form1";
			this->Text = L"Form1";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
		array <Point>^ points;
		Pen^ p;
		int id = 0;
		int mouseX;
		int mouseY;
		void clear()
		{
			id = 0;
			Graphics^ g = pictureBox1->CreateGraphics();
			g->Clear(System::Drawing::Color::FromArgb(255, 255, 255));
			points->Resize(points, 0);
		}
	private: System::Void pictureBox1_MouseClick(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e)
	{
		Graphics^ g = pictureBox1->CreateGraphics();
		points->Resize(points, id + 1);
		points[id].X = mouseX;
		points[id].Y = mouseY;
		id++;
		g->FillEllipse(Brushes::Red, mouseX, mouseY, 10, 10);
	}
	private: System::Void pictureBox1_MouseMove(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e)
	{
		mouseX = e->Location.X;
		mouseY = e->Location.Y;
		p = gcnew Pen(System::Drawing::Color::FromArgb(0, 0, 255));
	}
	private: System::Void conect(System::Object^ sender, System::EventArgs^ e)
	{
		Graphics^ g = pictureBox1->CreateGraphics();
		if (id >= 3)
		{
			int i;
			for(i = 1; i < id; i++) g->DrawLine(p, points[i - 1], points[i]);
			g->DrawLine(p, points[0], points[i - 1]);
		}
	}
	private: System::Void save(System::Object^ sender, System::EventArgs^ e)
	{
		std::ofstream out("newfile.txt", std::ios::app);
		for(int i = 0;i < id;i++)
			out << points[i].X << ' ' << points[i].Y << '\n';
	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e)
	{
		clear();
	}
	private: System::Void load(System::Object^ sender, System::EventArgs^ e)
	{
		clear();
		Graphics^ g = pictureBox1->CreateGraphics();
		std::string line;

		std::ifstream in("newfile.txt");
		if (in.is_open())
		{
			while (getline(in, line))
			{
				String^ strx = "";
				String^ stry = "";
				bool isx = true;
				for (int f = 0; f < line.length(); f++)
				{
					if (line[f] == ' ') { isx = false; f++; }
					if (isx) strx += Convert::ToChar(line[f]);
					else stry += Convert::ToChar(line[f]);
				}
				points->Resize(points, id + 1);
				points[id] = Point(Convert::ToInt16(strx), Convert::ToInt16(stry));
				g->FillEllipse(Brushes::Red, points[id].X, points[id].Y, 10, 10);
				id++;
			}
			if (id >= 3)
			{
				int i;
				p = gcnew Pen(System::Drawing::Color::FromArgb(0, 0, 255));
				for(i = 1;i < id;i++)
					g->DrawLine(p, points[i - 1], points[i]);
				g->DrawLine(p, points[0], points[i - 1]);
			}
		}
	}
	private: System::Void button9_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		int s = 0;
		int i;
		for (i = 1; i < id; i++) s += sqrt(pow(points[i].X - points[i - 1].X, 2) + 
			pow(points[i].Y - points[i - 1].Y, 2));
		s += sqrt(pow(points[0].X - points[i - 1].X, 2) +
			pow(points[0].Y - points[i - 1].Y, 2));
		Form^ dlg1 = gcnew Form();
		dlg1->Text = "�������� = " + Convert::ToString(s);
		dlg1->ShowDialog();
	}
	private: System::Void button10_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		float s1 = 0;
		float s2 = 0;
		for (int i = 1; i < id; i++) s1 += points[i - 1].X * points[i].Y;
		for (int i = 1; i < id; i++) s2 += points[i].X * points[i-1].Y;
		float s = 0.5 * (s1 - s2);
		Form^ dlg1 = gcnew Form();
		dlg1->Text = "������� = " + Convert::ToString(s);
		dlg1->ShowDialog();
	}
};
}