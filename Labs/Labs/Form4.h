#pragma once

namespace Labs {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form4
	/// </summary>
	public ref class Form4 : public System::Windows::Forms::Form
	{
	public:
		Form4(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form4()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	protected:
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::PictureBox^ pictureBox1;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::RichTextBox^ answers;



	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(Form4::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->answers = (gcnew System::Windows::Forms::RichTextBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(61, 13);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(90, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"������ �������";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(13, 36);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(59, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"������� X";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(144, 36);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(59, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"������� Y";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 52);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(60, 20);
			this->textBox1->TabIndex = 3;
			this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form4::textBox1_KeyPress);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(143, 52);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(60, 20);
			this->textBox2->TabIndex = 4;
			this->textBox2->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form4::textBox2_KeyPress);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->pictureBox1->Cursor = System::Windows::Forms::Cursors::Help;
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(16, 78);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(180, 56);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox1->TabIndex = 5;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Click += gcnew System::EventHandler(this, &Form4::pictureBox1_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(64, 160);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 6;
			this->button1->Text = L"������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form4::Summ);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(12, 144);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(189, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"������ �� ����� � ���������� ����";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(80, 206);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(0, 13);
			this->label5->TabIndex = 8;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(374, 13);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(88, 13);
			this->label6->TabIndex = 9;
			this->label6->Text = L"������ �������";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(289, 49);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(123, 23);
			this->button2->TabIndex = 10;
			this->button2->Text = L"������ if/else";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form4::null);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(429, 49);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(128, 23);
			this->button3->TabIndex = 11;
			this->button3->Text = L"������ switch";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form4::next);
			// 
			// answers
			// 
			this->answers->Location = System::Drawing::Point(299, 98);
			this->answers->Name = L"answers";
			this->answers->Size = System::Drawing::Size(240, 141);
			this->answers->TabIndex = 12;
			this->answers->Text = L"";
			// 
			// Form4
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(727, 261);
			this->Controls->Add(this->answers);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"Form4";
			this->Text = L"Laba 3";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void textBox1_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		System::Windows::Forms::TextBox^ tb = textBox1;
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (tb->TextLength != 0)
		{
			if ((tb->Text[tb->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox2_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		System::Windows::Forms::TextBox^ tb = textBox2;
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (tb->TextLength != 0)
		{
			if ((tb->Text[tb->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void Summ(System::Object^ sender, System::EventArgs^ e) 
	{
		double x = Convert::ToDouble(textBox1->Text);
		double y = Convert::ToDouble(textBox2->Text);
		if (((Math::Pow((x+1),2) + Math::Pow(y, 2) <= 1) && (y<=0))||((Math::Pow((x - 1), 2) + Math::Pow(y, 2) <= 1) && (y <= 0)))
			label5->Text = "������";
		else
			label5->Text = "�� ������";
	}
	private: System::Void null(System::Object^ sender, System::EventArgs^ e) 
	{
		double y;
		for (double x = 1, k = 0; x < 10; x++, k++)
		{
			if((x == 5)||(x == 7))
				y = x * x + 4 * x - 10;
			else if(x == 4)
				y = Math::Pow(4 * x, 1.0 / 2.0) + Math::Pow(16 * x, 1.0 / 3.0);
			else
				y = Math::Pow(x, 3) - 2;
			answers->Text += "x" + Convert::ToString(k) + " = " + Convert::ToString(x) + " | " + "y" + Convert::ToString(k) + " = " + Convert::ToString(y) + "\n";
		}
		answers->Text += "\n";
	}
	private: System::Void next(System::Object^ sender, System::EventArgs^ e) 
	{
		double y;
		int k = 0;
		for (int x = 1, k = 0; x < 10; x++, k++)
		{
			switch (x) 
			{
				case 5:
				case 7:
					y = x * x + 4 * x - 10;
					break;
				case 4:
					y = Math::Pow(4 * x, 1.0 / 2.0) + Math::Pow(16 * x, 1.0 / 3.0);
					break;
				default:
					y = Math::Pow(x, 3) - 2;
					break;
			}
			answers->Text += "x" + Convert::ToString(k) + " = " + Convert::ToString(x) + " | " + "y" + Convert::ToString(k) + " = " + Convert::ToString(y) + "\n";
		}
		answers->Text += "\n";
	}
	private: System::Void pictureBox1_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		textBox1->Text = Convert::ToString((float)(Cursor->Position.X - this->Location.X - 104)/13);
		textBox2->Text = Convert::ToString(-(float)(Cursor->Position.Y - this->Location.Y - 145)/13);
	}
};
}
