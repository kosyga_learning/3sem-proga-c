#pragma once

namespace Labs {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form3
	/// </summary>
	public ref class Form3 : public System::Windows::Forms::Form
	{
	public:
		Form3(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form3()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	protected:
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::RichTextBox^ richTextBox1;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::TextBox^ textBox4;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Label^ label8;


	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(241, 203);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(120, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"������� ������ �����";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(135, 196);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 1;
			this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form3::textBox1_KeyPress);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(135, 222);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 3;
			this->textBox2->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form3::textBox2_KeyPress);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(241, 225);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(138, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"������� ��������� �����";
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(135, 248);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(35, 20);
			this->textBox3->TabIndex = 5;
			this->textBox3->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form3::textBox3_KeyPress);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(176, 255);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(71, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"������� ���";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 246);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(117, 23);
			this->button1->TabIndex = 6;
			this->button1->Text = L"��������� ��";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form3::Summ);
			// 
			// richTextBox1
			// 
			this->richTextBox1->Location = System::Drawing::Point(12, 40);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->Size = System::Drawing::Size(117, 202);
			this->richTextBox1->TabIndex = 7;
			this->richTextBox1->Text = L"";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(157, 155);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(55, 13);
			this->label4->TabIndex = 8;
			this->label4->Text = L"�������";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(154, 172);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(64, 13);
			this->label5->TabIndex = 9;
			this->label5->Text = L"y=2x^2-5x-8";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(28, 13);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(90, 13);
			this->label6->TabIndex = 10;
			this->label6->Text = L"������ �������";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(431, 13);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(88, 13);
			this->label7->TabIndex = 11;
			this->label7->Text = L"������ �������";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(431, 36);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(283, 13);
			this->label9->TabIndex = 14;
			this->label9->Text = L"������� ����� ������ �����, ������� ������ �������";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(434, 52);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(100, 20);
			this->textBox4->TabIndex = 15;
			this->textBox4->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form3::textBox4_KeyPress);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(434, 87);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 16;
			this->button2->Text = L"�������";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form3::null);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(434, 134);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(0, 13);
			this->label8->TabIndex = 17;
			// 
			// Form3
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(747, 280);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->richTextBox1);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label1);
			this->Name = L"Form3";
			this->Text = L"Laba 2";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Summ(System::Object^ sender, System::EventArgs^ e) 
	{
		double start = Convert::ToDouble(textBox1->Text);
		double finish = Convert::ToDouble(textBox2->Text);
		double step = Convert::ToDouble(textBox3->Text);
		for (double x = start; x < finish; x += step)
		{
			richTextBox1->Text += "y = " + Convert::ToString(2 * pow(x, 2) - 5 * x - 8) + "\n";
		}
	}
private: System::Void textBox1_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
{
	System::Windows::Forms::TextBox^ tb = textBox1;
	if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
	if (tb->TextLength != 0)
	{
		if ((tb->Text[tb->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
		if ((isspace(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
		if ((Char::IsDigit(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
	}
}
private: System::Void textBox2_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
{
	System::Windows::Forms::TextBox^ tb = textBox2;
	if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
	if (tb->TextLength != 0)
	{
		if ((tb->Text[tb->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
		if ((isspace(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
		if ((Char::IsDigit(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
	}
}
private: System::Void textBox3_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
{
	System::Windows::Forms::TextBox^ tb = textBox3;
	if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
	if (tb->TextLength != 0)
	{
		if ((tb->Text[tb->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
		if ((isspace(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
		if ((Char::IsDigit(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
	}
}
private: System::Void textBox4_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e)
{
	System::Windows::Forms::TextBox^ tb = textBox4;
	if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
	if (tb->TextLength != 0)
	{
		if ((tb->Text[tb->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
		if ((isspace(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
		if ((Char::IsDigit(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
	}
}
private: System::Void null(System::Object^ sender, System::EventArgs^ e) 
{
	int i=1;
	double s = Convert::ToDouble(textBox4->Text[0]);
	while (Convert::ToDouble(textBox4->TextLength) > i)
	{
		s *= Convert::ToDouble(Convert::ToString(textBox4->Text[i]));
		i++;
	}
	label8->Text = "����� = " + Convert::ToString(s);
}
};
}
