#pragma once
#include "Form2.h"
#include "Form3.h"
#include "Form4.h"
#include "Form5.h"
#include "Form6.h"
#include "Form7.h"
#include "Form8.h"

namespace Labs {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Button^ button6;
	private: System::Windows::Forms::Button^ button7;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button1->Location = System::Drawing::Point(12, 12);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Laba 1";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::Summ);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button2->Location = System::Drawing::Point(12, 42);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Laba 2";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::null);
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button3->Location = System::Drawing::Point(12, 72);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Laba 3";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::next);
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button4->Location = System::Drawing::Point(12, 102);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 3;
			this->button4->Text = L"Laba 4";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// button5
			// 
			this->button5->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button5->Location = System::Drawing::Point(12, 132);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(75, 23);
			this->button5->TabIndex = 4;
			this->button5->Text = L"Laba 5";
			this->button5->UseVisualStyleBackColor = false;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// button6
			// 
			this->button6->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button6->Location = System::Drawing::Point(12, 162);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(75, 23);
			this->button6->TabIndex = 5;
			this->button6->Text = L"Laba 6";
			this->button6->UseVisualStyleBackColor = false;
			this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
			// 
			// button7
			// 
			this->button7->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->button7->Location = System::Drawing::Point(12, 191);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(75, 23);
			this->button7->TabIndex = 6;
			this->button7->Text = L"Laba 7";
			this->button7->UseVisualStyleBackColor = false;
			this->button7->Click += gcnew System::EventHandler(this, &Form1::button7_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Name = L"Form1";
			this->Text = L"LabsHub";
			this->ResumeLayout(false);

		}
#pragma endregion
	public: System::EventArgs^ textboxNoSpace(System::Windows::Forms::KeyPressEventArgs^ e, System::Windows::Forms::TextBox^ tb)
	{
		if ((tb->TextLength == 0) && (isspace(e->KeyChar))) e->KeyChar = 0;
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (!isspace(e->KeyChar)) && (e->KeyChar != ',') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (tb->TextLength != 0)
		{
			if ((tb->Text[tb->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar != ',') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
		return e;
	}
	public: System::EventArgs^ textboxSpace(System::Windows::Forms::KeyPressEventArgs^ e, System::Windows::Forms::TextBox^ tb)
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != ',') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (tb->TextLength != 0)
		{
			if ((tb->Text[tb->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar != ',') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(tb->Text[tb->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
		return e;
	}
	private: System::Void Summ(System::Object^ sender, System::EventArgs^ e) 
	{
		Form2^ f2 = gcnew Form2();
		f2->Show();
	}
	private: System::Void null(System::Object^ sender, System::EventArgs^ e) 
	{
		Form3^ f3 = gcnew Form3();
		f3->Show();
	}
private: System::Void next(System::Object^ sender, System::EventArgs^ e) 
	{
		Form4^ f4 = gcnew Form4();
		f4->Show();
	}
private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Form5^ f5 = gcnew Form5();
		f5->Show();
	}
private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Form6^ f6 = gcnew Form6();
		f6->Show();
	}
private: System::Void button6_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Form7^ f7 = gcnew Form7();
		f7->Show();
	}
	private: System::Void button7_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		Form8^ f8 = gcnew Form8();
		f8->Show();
	}
};
}
