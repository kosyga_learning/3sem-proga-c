#pragma once
#include <functional>
#include <xstring>
#include <xhash>
#include <array>
#include <unordered_map>
#include <iterator>

namespace Labs {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form7
	/// </summary>
	public ref class Form7 : public System::Windows::Forms::Form
	{
	public:
		Form7(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form7()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ button1;
	protected:
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::Label^ label4;

	private: System::Windows::Forms::RichTextBox^ richTextBox1;

	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::TextBox^ textBox4;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::RichTextBox^ richTextBox2;
	private: System::Windows::Forms::CheckBox^ checkBox1;
	private: System::Windows::Forms::TextBox^ textBox5;
	private: System::Windows::Forms::TextBox^ textBox6;
	private: System::Windows::Forms::TextBox^ textBox7;
	private: System::Windows::Forms::TextBox^ textBox8;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::RichTextBox^ richTextBox3;

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->richTextBox2 = (gcnew System::Windows::Forms::RichTextBox());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->richTextBox3 = (gcnew System::Windows::Forms::RichTextBox());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 86);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"��������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form7::button1_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(13, 24);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 1;
			this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form7::textBox1_KeyPress);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(13, 60);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 2;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 44);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(93, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"������� �������";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(15, 5);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(81, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"������� �����";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(14, 137);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(58, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"���������";
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(222, 24);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 6;
			this->textBox3->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form7::textBox3_KeyPress);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(193, 5);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(161, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"������� ������ ����� ������";
			// 
			// richTextBox1
			// 
			this->richTextBox1->Location = System::Drawing::Point(222, 115);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->Size = System::Drawing::Size(100, 96);
			this->richTextBox1->TabIndex = 9;
			this->richTextBox1->Text = L"";
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(222, 86);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(100, 23);
			this->button3->TabIndex = 11;
			this->button3->Text = L"�����������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form7::button3_Click);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(436, 5);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(86, 13);
			this->label6->TabIndex = 12;
			this->label6->Text = L"������� ������";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(422, 24);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(100, 20);
			this->textBox4->TabIndex = 13;
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(439, 58);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 14;
			this->button4->Text = L"��������";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form7::button4_Click);
			// 
			// richTextBox2
			// 
			this->richTextBox2->Location = System::Drawing::Point(422, 115);
			this->richTextBox2->Name = L"richTextBox2";
			this->richTextBox2->Size = System::Drawing::Size(100, 96);
			this->richTextBox2->TabIndex = 15;
			this->richTextBox2->Text = L"";
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Location = System::Drawing::Point(212, 50);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(110, 17);
			this->checkBox1->TabIndex = 16;
			this->checkBox1->Text = L"�� �����������";
			this->checkBox1->UseVisualStyleBackColor = true;
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(612, 24);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(32, 20);
			this->textBox5->TabIndex = 17;
			this->textBox5->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form7::textBox5_KeyPress);
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(679, 24);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(32, 20);
			this->textBox6->TabIndex = 18;
			this->textBox6->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form7::textBox6_KeyPress);
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(746, 24);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(32, 20);
			this->textBox7->TabIndex = 19;
			this->textBox7->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form7::textBox7_KeyPress);
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(803, 24);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(32, 20);
			this->textBox8->TabIndex = 20;
			this->textBox8->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form7::textBox8_KeyPress);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(650, 27);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(23, 13);
			this->label5->TabIndex = 21;
			this->label5->Text = L"X +";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(717, 27);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(23, 13);
			this->label7->TabIndex = 22;
			this->label7->Text = L"X +";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(660, 14);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(13, 13);
			this->label8->TabIndex = 23;
			this->label8->Text = L"2";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(784, 27);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(13, 13);
			this->label9->TabIndex = 24;
			this->label9->Text = L"=";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(679, 60);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 25;
			this->button2->Text = L"������";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form7::button2_Click_1);
			// 
			// richTextBox3
			// 
			this->richTextBox3->Location = System::Drawing::Point(663, 115);
			this->richTextBox3->Name = L"richTextBox3";
			this->richTextBox3->Size = System::Drawing::Size(100, 96);
			this->richTextBox3->TabIndex = 26;
			this->richTextBox3->Text = L"";
			// 
			// Form7
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(969, 261);
			this->Controls->Add(this->richTextBox3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->textBox8);
			this->Controls->Add(this->textBox7);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->checkBox1);
			this->Controls->Add(this->richTextBox2);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->richTextBox1);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->button1);
			this->Name = L"Form7";
			this->Text = L"Form7";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void textBox1_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox1->TextLength != 0)
		{
			if ((textBox1->Text[textBox1->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox1->Text[textBox1->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox1->Text[textBox1->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Double power(double num, double p)
	{
		return (exp(p * log(num)));
	}
	private: std::vector<Char> invert(std::vector<Char> Array, int size)
	{
		std::vector<Char> new_array;
		new_array.resize(size);
		for (int i = size-1, j = 0; i >= 0; i--, j++) {
			new_array[j] = Array[i];
		}
		return new_array;
	}
	private: std::vector<double> sortVoz(std::vector<double> array, int size)
	{
		for (int i = 0; i < size - 1; i++)
		{
			int min_index = i;
			for (int j = i + 1; j < size; j++)
				if (array[j] < array[min_index])
					min_index = j;
			if (min_index != i)
				std::swap(array[i], array[min_index]);
		}
		return array;
	}
	private: std::vector<double> sortYb(std::vector<double> array, int size)
	{
		int t;
		for (int i = 0; i < size - 1; i++)
			for (int j = 0; j < size - i - 1; j++)
				if (array[j] < array[j + 1]) {
					t = array[j];
					array[j] = array[j + 1];
					array[j + 1] = t;
				}
		return array;
	}
	private: String^ quad(double k1, double k2, double k3, double k4)
	{
		double c4 = k3 - k4;
		double x1;
		double x2;
		double d = k2 * k2 - 4 * k1 * c4;
		String^ answer;
		//k1 * x * x + k2 * x + k3 - k4 = 0
		if (d > 0)
		{
			x1 = (-k2 + Math::Pow(d, 1 / 2)) / (2 * k1);
			x2 = (-k2 - Math::Pow(d, 1 / 2)) / (2 * k1);
			answer = "X1 = " + x1 + "\n" + "X2 = " + x2;
		}
		else if (d == 0)
		{
			x1 = -k2 / (2 * k1);
			answer = "��� ��� D = 0" + "\n" + "X1 = " + x1;
		}
		else answer = "��� ������";
		return answer;
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		label3->Text = textBox1->Text + " � ������� " + textBox2->Text + " = " + Convert::ToString(power(Convert::ToDouble(textBox1->Text), Convert::ToDouble(textBox2->Text)));
	}
	private: System::Void textBox3_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if ((textBox3->TextLength == 0) && (isspace(e->KeyChar))) e->KeyChar = 0;
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (!isspace(e->KeyChar) && (e->KeyChar != '\b'))) e->KeyChar = 0;
		if (textBox3->TextLength != 0)
		{
			if ((textBox3->Text[textBox3->TextLength - 1] == '-') && ((e->KeyChar == '.') || (isspace(e->KeyChar)) || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox3->Text[textBox3->TextLength - 1])) && ((e->KeyChar == '.') || (isspace(e->KeyChar)))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox3->Text[textBox3->TextLength - 1])) && (e->KeyChar == '-')) e->KeyChar = 0;
		}
	}
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		if (textBox3->Text[textBox3->TextLength - 1] != ' ') textBox3->Text += " ";
		int n = 0;
		String^ num;
		std::vector<double> array;
		for (int i = 0; i < textBox3->TextLength; i++)
			if (isspace(textBox3->Text[i])) n++;
		array.resize(n);
		for (int i = 0, j = 0; i < textBox3->TextLength; i++)
			if (!isspace(textBox3->Text[i]))
				num+= textBox3->Text[i];
			else
			{
				array[j] = Convert::ToDouble(num);
				num = "";
				j++;
			}
		std::sort(array.begin(), array.end());
		richTextBox1->Text = "��� ��� ������ ��������������� � ������� �����������:" + "\n";
		for (int i = 0; i < n; i++)
			richTextBox1->Text += array[i] + " ";
	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		if (textBox3->Text[textBox3->TextLength - 1] != ' ') textBox3->Text += " ";
		int n = 0;
		String^ num;
		String^ VU = "�����������";
		std::vector<double> array;
		for (int i = 0; i < textBox3->TextLength; i++)
			if (isspace(textBox3->Text[i])) n++;
		array.resize(n);
		for (int i = 0, j = 0; i < textBox3->TextLength; i++)
			if (!isspace(textBox3->Text[i]))
				num += textBox3->Text[i];
			else
			{
				array[j] = Convert::ToDouble(num);
				num = "";
				j++;
			}
		if (checkBox1->Checked)
			array = sortVoz(array, n);
		else
		{
			array = sortYb(array, n);
			VU = "��������";
		}
		richTextBox1->Text = "��� ��� ������, ��������������� � ������� " + VU + ":" + "\n";
		for (int i = 0; i < n; i++)
			richTextBox1->Text += array[i] + " ";
	}
	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		if (textBox4->Text[textBox4->TextLength - 1] != ' ') textBox4->Text += " ";
		std::vector<Char> text;
		text.resize(textBox4->TextLength);
		for (int i = 0; i < textBox4->TextLength; i++)
			text[i] = textBox4->Text[i];
		text = invert(text, textBox4->TextLength);
		richTextBox2->Text = "��� ���� ������ ��������:" + "\n";
		for (int i = 0; i < textBox4->TextLength; i++)
			richTextBox2->Text += text[i];
	}
	private: System::Void button2_Click_1(System::Object^ sender, System::EventArgs^ e) 
	{
		richTextBox3->Text = quad(Convert::ToDouble(textBox5->Text),Convert::ToDouble(textBox6->Text),Convert::ToDouble(textBox7->Text),Convert::ToDouble(textBox8->Text));
	}
	private: System::Void textBox5_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox5->TextLength != 0)
		{
			if ((textBox5->Text[textBox5->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox5->Text[textBox5->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox5->Text[textBox5->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox6_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox6->TextLength != 0)
		{
			if ((textBox6->Text[textBox6->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox6->Text[textBox6->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox6->Text[textBox6->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox7_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox7->TextLength != 0)
		{
			if ((textBox7->Text[textBox7->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox7->Text[textBox7->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox7->Text[textBox7->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
	private: System::Void textBox8_KeyPress(System::Object^ sender, System::Windows::Forms::KeyPressEventArgs^ e) 
	{
		if ((!System::Char::IsDigit(e->KeyChar)) && (e->KeyChar != '.') && (e->KeyChar != '-') && (e->KeyChar != '\b')) e->KeyChar = 0;
		if (textBox8->TextLength != 0)
		{
			if ((textBox8->Text[textBox8->TextLength - 1] == '-') && ((e->KeyChar == '.') || (e->KeyChar == '-'))) e->KeyChar = 0;
			if ((isspace(textBox8->Text[textBox8->TextLength - 1])) && ((e->KeyChar == '.'))) e->KeyChar = 0;
			if ((Char::IsDigit(textBox8->Text[textBox8->TextLength - 1])) && ((e->KeyChar == '-'))) e->KeyChar = 0;
		}
	}
};
}
