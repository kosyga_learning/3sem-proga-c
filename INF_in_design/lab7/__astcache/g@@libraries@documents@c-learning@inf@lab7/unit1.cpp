﻿//---------------------------------------------------------------------------

#include <vcl.h>
#include <fstream>
#include <iostream>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
std::string path;
void __fastcall TForm1::Edit1Change(TObject *Sender)
{
	path = AnsiString(Edit1->Text).c_str();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	if(std::filesystem::exists(path))
	{
		std::ifstream file(path);
		std::string line;
		Memo1->Text = "";
		if (file.is_open())
			while (getline(file, line)) Memo1->Text += AnsiString(line.c_str()) + '\n';
		file.close();
	}
	else ShowMessage("File is not exists");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	if(std::filesystem::exists(path))
	{
		const char* newFileName = AnsiString(InputBox("Copy", "Path", Edit1->Text)).c_str();
		if(std::filesystem::exists(newFileName))
		{
			std::ifstream fin(path);
			std::ofstream fout(newFileName);
			std::string line;
			if (fin.is_open())
				while (getline(fin, line)) fout << line << "\n";
			fin.close();
            fout.close();
		}
		else ShowMessage("File is not exists");
	}
	else ShowMessage("File is not exists");
}
//---------------------------------------------------------------------------

