object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 439
  ClientWidth = 616
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  object Label1: TLabel
    Left = 296
    Top = 8
    Width = 24
    Height = 15
    Caption = 'Path'
  end
  object Edit1: TEdit
    Left = 89
    Top = 24
    Width = 450
    Height = 23
    TabOrder = 0
    Text = 'Edit1'
    OnChange = Edit1Change
  end
  object Button1: TButton
    Left = 8
    Top = 23
    Width = 75
    Height = 25
    Caption = 'Show'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 545
    Top = 23
    Width = 75
    Height = 25
    Caption = 'Copy'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 54
    Width = 612
    Height = 380
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    TabOrder = 3
  end
end
