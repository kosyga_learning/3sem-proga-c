﻿//---------------------------------------------------------------------------

#include <vcl.h>
#include <stdio.h>
#include "Windows.h"
#include "WtsApi32.h"
#include <winternl.h>
#pragma hdrstop
#pragma comment(lib,"ntdll.lib") // Need to link with ntdll.lib import library. You can find the ntdll.lib from the Windows DDK.

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
UnicodeString BTS(bool b)
{
  return b ? "true" : "false";
}
int id;
int idt;
int t;
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	WTS_PROCESS_INFO* pWPIs = NULL;
	DWORD dwProcCount = 0;
	if (WTSEnumerateProcesses(WTS_CURRENT_SERVER_HANDLE, NULL, 1, &pWPIs, &dwProcCount))
	{

		for (DWORD i = 0; i < dwProcCount; i++) {
			ListBox1->Items->Add(pWPIs[i].pProcessName);
		}
	}

	if (pWPIs)
	{
		WTSFreeMemory(pWPIs);
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListBox1Click(TObject *Sender)
{
    WTS_PROCESS_INFO* pWPIs = NULL;
	DWORD dwProcCount = 0;
	if (WTSEnumerateProcesses(WTS_CURRENT_SERVER_HANDLE, NULL, 1, &pWPIs, &dwProcCount))
	{
		id = pWPIs[ListBox1->ItemIndex].ProcessId;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	const auto explorer = OpenProcess(PROCESS_TERMINATE, false, id);
	TerminateProcess(explorer, 1);
	CloseHandle(explorer);
}
//---------------------------------------------------------------------------
bool CanClose = true;
void __fastcall TForm1::Button1Click(TObject *Sender)
{
    CanClose = false;
    TrayIcon1->Visible = true;
	ShowWindow(Form1->Handle, SW_HIDE);

}
//---------------------------------------------------------------------------
void __fastcall TForm1::TrayIcon1Click(TObject *Sender)
{
    TrayIcon1->Visible = false;
    ShowWindow(Form1->Handle, SW_SHOW);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
    ListBox1->Items->Clear();
    WTS_PROCESS_INFO* pWPIs = NULL;
	DWORD dwProcCount = 0;
	if (WTSEnumerateProcesses(WTS_CURRENT_SERVER_HANDLE, NULL, 1, &pWPIs, &dwProcCount))
	{
		for (DWORD i = 0; i < dwProcCount; i++) {
			ListBox1->Items->Add(pWPIs[i].pProcessName);
		}
	}

	if (pWPIs)
	{
		WTSFreeMemory(pWPIs);
	}

	bool note = false;
	for(int i = 0; i < dwProcCount ;i++)
	{
		AnsiString _astr = ListBox1->Items->Strings[i];
		std::string s1 = _astr.c_str();
		if(s1.find("notepad"))
		{
			note = true;
			idt = pWPIs[i].ProcessId;
		}
	}
	if(note)
	{
		t+=30;
		if(t==120)
		{
			const auto explorer = OpenProcess(PROCESS_TERMINATE, false, idt);
			TerminateProcess(explorer, 1);
			CloseHandle(explorer);
        }
	}
	else t = 0;
	ShowMessage(BTS(note));
}
//---------------------------------------------------------------------------

