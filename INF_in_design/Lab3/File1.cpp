﻿#include <string>
#include <iostream>
#include <vector>
#include <cstring>
#include <fstream>
#include <windows.h>

void task1() 
{
    int n,k = 0;
    std::cin >> n;
    while (n>0)
    {
        n /= 10;
        k++;
    }
    std::cout << "В данном числе " << k << " цифр" << "\n";
}
void task2()
{
    // Объявляю этот сраный динамический двумерный массив
    int N, M;
    std::cout << "Введите размер массива" << '\n';
    std::cin >> N >> M;
    int *ar = new int[M];
    int **array = new int*[N];
    for (int i = 0; i < N; i++)
    {
        array[i] = new int[M];
        for (int j = 0; j < M; j++)//инициализирую его
        {
            std::cout << "Введите " << j << " элемент " << i << " строки" << '\n';
            std::cin >> array[i][j];
        }
    }

    //если столб = 0 эл = 0
    for (int j = 0; j < M; j++)
    {
        bool t = true;
        for (int i = 0; i < N; i++)
            if (array[i][j] != 0) t = false;
        if (t) ar[j] = 0;
        else ar[j] = 1;
    }

    //вывожу массив
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
            std::cout << array[i][j] << " ";
        std::cout << "\n";
    }
    std::cout << "\n";
    //вывожу однм массив
    for (int i = 0; i < M; i++)
        std::cout << ar[i] << " ";
        
    //освобождаю память
    for (int i = 0; i < N; i++)
        delete[] array[i];
    delete[] array; 
}
void task3()
{
    std::string text;
    std::string newText;
    char sign;
    std::cout << "Введите символ" << '\n';
    std::cin >> sign;
    std::cout << "Введите текст, в конце поставьте;" << '\n';
    getline(std::cin, text, ';');
    text = ' ' + text;
    
    for (int i = 1; i < text.length(); i++)
        if ((text[i] == sign)&&(!isalpha(text[i-1])))
            while (isalpha(text[i])) i++;
        else newText += text[i];
    
    std::cout << newText << std::endl;
}
void task4()
{
    std::ifstream file("indTask4.txt");
    std::string line[24];
    std::string word[2];
    int i = 0;
    if (file.is_open())
        while (getline(file, line[i])) i++;
    file.close();
    for (int j = 0; !isspace(line[1][j]); j++)
        if(!ispunct(line[1][j])) word[0] += line[1][j];
    for (int j = 0; j < line[4].length(); j++)
    {
        if (isalpha(line[4][j])) word[1] += line[4][j];
        if (isspace(line[4][j])) word[1].resize(0);
    }
    std::cout << word[0] << '\n' << word[1] << '\n';
    int t = 0;
    if (word[0] == word[1]) std::cout << "Слова совпадают" << '\n';
    else std::cout << "Слова не совпадают" << '\n';
}
void task5()
{
    std::ifstream file("indTask5.txt");
    std::string line;
    std::string bufer;
    int minDigit = 999;
    int n=0;
    int maxDigit = -999;
    if (file.is_open()) getline(file, line);
    file.close();
    line += ' ';
    for (int i = 0; i != line.length(); i++)
        if (isspace(line[i])) n++;
    int *digits= new int[n];
    for (int i = 0, k = 0; line[i]!= NULL; i++)
        if (!isspace(line[i]))
            bufer += line[i];
        else
        {
            digits[k] = std::atoi(bufer.c_str());
            k++;
            bufer.resize(0);
        }
    for (int i = 0; i < n; i++)
    {
        if (digits[i] < minDigit) minDigit = digits[i];
        if (digits[i] > maxDigit) maxDigit = digits[i];
    }
    std::cout << "min = " << minDigit << '\n' << "max = " << maxDigit << '\n';
}

int main()
{
    int n;
    setlocale(LC_ALL, "rus");
    std::cout << "Введите номер задания" << "\n";
    std::cin >> n;
    switch (n) 
    {
        case 1: task1();
            break;
        case 2: task2();
            break;
        case 3: task3();
            break;
        case 4: task4();
            break;
        case 5: task5();
            break;
        default: std::cout << "Такого задания нет" << "\n";
    }
}