//---------------------------------------------------------------------------

#include <vcl.h>
#include <DateUtils.hpp>
#include <Math.hpp>

#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	IdTime1->Host = LabeledEdit1->Text;
	AnsiString tdv = IdTime1->DateTime.DateTimeString();

	IdTime1->Disconnect();
	int SpacePosition = tdv.AnsiPos(" ");
	AnsiString td = tdv.SubString(1, SpacePosition-1);
	LabeledEdit2->Text = td;
	AnsiString tv = tdv.SubString(SpacePosition+1, tdv.Length());
	LabeledEdit3->Text = tv;

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	AnsiString cmd = "date " + LabeledEdit2->Text;
	system(cmd.c_str());
	AnsiString cmd2 = "time " + LabeledEdit3->Text;
	system(cmd2.c_str());
	Timer1->Enabled = true;
	LabeledEdit2->Clear();
	LabeledEdit3->Clear();
	Form1->Button1->SetFocus();
	//Image1->Canvas->Brush->Color = clBtnFace;
	//Image1->Canvas->FillRect(Rect(0,0,Image1->Width, Image1->Height));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	int x_c = Image1->Width/2;
	int y_c = Image1->Height/2;
	Image1->Picture->LoadFromFile("clock.bmp");
	//Image1->Canvas->Ellipse(0,0, Image1->Width, Image1->Height);

	float DlinaStrelki = Image1->Width/2-5;
	float alpha = 90. - 360/60 * SecondOf(Time());
	float x = x_c + DlinaStrelki*cos(DegToRad(alpha));
	float y = y_c - DlinaStrelki*sin(DegToRad(alpha));
	Image1->Canvas->Pen->Color = clRed;
	Image1->Canvas->MoveTo(x_c, y_c);
	Image1->Canvas->LineTo(x, y);

	DlinaStrelki = Image1->Width/2-10;
	alpha = 90. - 360/60 * MinuteOf(Time());
	float x1 = x_c + DlinaStrelki*cos(DegToRad(alpha));
	float y1 = y_c - DlinaStrelki*sin(DegToRad(alpha));
	Image1->Canvas->Pen->Color = clBlack;
	Image1->Canvas->MoveTo(x_c, y_c);
	Image1->Canvas->LineTo(x1, y1);

	DlinaStrelki = Image1->Width/2-20;
	alpha = 90. - 360/12 * HourOf(Time());
	float x2 = x_c + DlinaStrelki*cos(DegToRad(alpha));
	float y2 = y_c - DlinaStrelki*sin(DegToRad(alpha));
	Image1->Canvas->Pen->Color = clBlack;
	Image1->Canvas->MoveTo(x_c, y_c);
	Image1->Canvas->LineTo(x2, y2);

	Label1->Caption=Time();


}
//---------------------------------------------------------------------------
