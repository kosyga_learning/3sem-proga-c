object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object Label1: TLabel
    Left = 256
    Top = 91
    Width = 24
    Height = 15
    Caption = 'path'
  end
  object Edit1: TEdit
    Left = 104
    Top = 112
    Width = 321
    Height = 23
    TabOrder = 0
    OnChange = Edit1Change
  end
  object Button1: TButton
    Left = 136
    Top = 141
    Width = 75
    Height = 25
    Caption = 'Create folder'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 329
    Top = 141
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 232
    Top = 141
    Width = 75
    Height = 25
    Caption = 'Rename'
    TabOrder = 3
    OnClick = Button3Click
  end
end
