//---------------------------------------------------------------------------

#include <vcl.h>
#include<iostream>
#include <filesystem>
#include <cstdio>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
std::string path;
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	if(!std::filesystem::exists(path)) std::filesystem::create_directories(path);
	else ShowMessage("File is already exists");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit1Change(TObject *Sender)
{
	path = AnsiString(Edit1->Text).c_str();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	if(std::filesystem::exists(path)) std::filesystem::remove_all(path);
	else ShowMessage("File is not exists");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	if(std::filesystem::exists(path))
	{
		const char* oldFileName = path.c_str();
		const char* newFileName = AnsiString(InputBox("Rename", "Name", Edit1->Text)).c_str();
		if(std::filesystem::exists(newFileName))
		{
			std::rename(oldFileName, newFileName);
			Edit1->Text = newFileName;
		}
		else ShowMessage("File is not exists");
	}
	else ShowMessage("File is not exists");
}
//---------------------------------------------------------------------------

