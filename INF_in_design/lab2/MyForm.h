#pragma once
#include <string>
#include <stdlib.h>
#include <tchar.h>
#include <atlstr.h>
#include <map>
#include <Windows.h>
#include <winbase.h>
#include <stdio.h>
#include <stdlib.h>
#include <Lmcons.h>
#include <array>
#include <Intrin.h>
#define DIV 1048576
#if defined(_WIN32)
#define PLATFORM_NAME "windows" // Windows
#elif defined(_WIN64)
#define PLATFORM_NAME "windows" // Windows
#elif defined(__CYGWIN__) && !defined(_WIN32)
#define PLATFORM_NAME "windows" // Windows (Cygwin POSIX under Microsoft Window)
#elif defined(__ANDROID__)
#define PLATFORM_NAME "android" // Android (implies Linux, so it must come first)
#elif defined(__linux__)
#define PLATFORM_NAME "linux" // Debian, Ubuntu, Gentoo, Fedora, openSUSE, RedHat, Centos and other
#elif defined(__unix__) || !defined(__APPLE__) && defined(__MACH__)
#include <sys/param.h>
#if defined(BSD)
#define PLATFORM_NAME "bsd" // FreeBSD, NetBSD, OpenBSD, DragonFly BSD
#endif
#elif defined(__hpux)
#define PLATFORM_NAME "hp-ux" // HP-UX
#elif defined(_AIX)
#define PLATFORM_NAME "aix" // IBM AIX
#elif defined(__APPLE__) && defined(__MACH__) // Apple OSX and iOS (Darwin)
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR == 1
#define PLATFORM_NAME "ios" // Apple iOS
#elif TARGET_OS_IPHONE == 1
#define PLATFORM_NAME "ios" // Apple iOS
#elif TARGET_OS_MAC == 1
#define PLATFORM_NAME "osx" // Apple OSX
#endif
#elif defined(__sun) && defined(__SVR4)
#define PLATFORM_NAME "solaris" // Oracle Solaris, Open Indiana
#else
#define PLATFORM_NAME NULL
#endif

// Return a name of platform, if determined, otherwise - an empty string


namespace $safeprojectname$ {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::RichTextBox^ richTextBox1;
	private: System::Windows::Forms::Button^ button1;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// richTextBox1
			// 
			this->richTextBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->richTextBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.48F));
			this->richTextBox1->Location = System::Drawing::Point(0, 1);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->ReadOnly = true;
			this->richTextBox1->Size = System::Drawing::Size(281, 220);
			this->richTextBox1->TabIndex = 0;
			this->richTextBox1->Text = L"";
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(0, 237);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(281, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"����";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->richTextBox1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);

		}
#pragma endregion
	const char* get_platform_name()
	{
		return (PLATFORM_NAME == NULL) ? "" : PLATFORM_NAME;
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e)
	{
		richTextBox1->Clear();
		const int N = 256;
		unsigned long int CN = 256;
		char str[N];
		//BIOS
		
		//OS
		GetEnvironmentVariable(LPCTSTR("OS"), LPTSTR(str), N); //os � ��������� ����������
		String^ sBuffer = gcnew String(str);
		richTextBox1->Text += "Platforma OS: " + gcnew String(get_platform_name()) + "\n";
		//CPU
		SYSTEM_INFO siSysInfo;
		GetSystemInfo(&siSysInfo);
		richTextBox1->Text += "CPU Type: "+ "\n" + "������: " + siSysInfo.wProcessorLevel + " ������: " + siSysInfo.wProcessorRevision + "\n";
		//RAM
		MEMORYSTATUSEX statex;
		statex.dwLength = sizeof(statex);
		GlobalMemoryStatusEx(&statex);
		richTextBox1->Text += "Memory: " + statex.ullTotalPhys / 1000 / DIV + "��" + " (" + statex.ullTotalPhys / DIV +") " + "\n";
		richTextBox1->Text += "��������: " + statex.ullAvailPhys / DIV + "�� ������: " +  statex.dwMemoryLoad + "%" + "\n";
		//NAMEs
		GetComputerNameA(str, &CN);
		sBuffer = gcnew String(str);
		richTextBox1->Text += "Computer name: " + sBuffer + "\n";
		GetUserNameA(str, &CN);
		sBuffer = gcnew String(str);
		richTextBox1->Text += "Username: " + sBuffer + "\n";
		//DISKS
		richTextBox1->Text += "Disk info:" + "\n";
		LPCTSTR p[3];
		Char chDisk[3];
		chDisk[0] = 'C';
		chDisk[1] = 'G';
		chDisk[2] = 'A';
		p[0] = L"C:\\";
		p[1] = L"G:\\";
		p[2] = L"A:\\";
		for (int i = 0; i < 3; i++)
		{
			ULARGE_INTEGER FreeBytesToCaller, TotalBytes, FreeBytes;
			GetDiskFreeSpaceEx(p[i], &FreeBytesToCaller, &TotalBytes, &FreeBytes);
			richTextBox1->Text += "�����: " + chDisk[i].ToString() + "\n";
			richTextBox1->Text += "�����: " + TotalBytes.QuadPart / 1073741824 + " ��" + "\n";
			richTextBox1->Text += "��������: " + FreeBytesToCaller.QuadPart / 1073741824 + " ��" + "\n" + "\n";
		}
	}
	};
}
