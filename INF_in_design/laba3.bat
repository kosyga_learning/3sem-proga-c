echo off
cls

echo -------------------------------------- 
echo ---------- .:: -=LEXX=- ::. ----------
echo -------------------------------------- 
echo ------ %date% [%time%] ------
echo -------------------------------------- 

:: -------------------------------------- ::
::     Имена исходников (через пробел)    ::
:: -------------------------------------- ::
set gavSrc=MySource1.cpp MySource2.cpp


:: -------------------------------------- ::
:: Дополнительно, если необходимо, для Qt ::
:: - заголовочный файл,содержащий Q_OBJECT::
:: из него будет сгенерирован moc_xxx.cpp ::
:: -------------------------------------- ::
::set gavSrcQtMoc=gavQtSignalSlot.h
set gavSrcQtMoc=""

:: -------------------------------------- ::
:: Версия компилятора:                    ::
::    6 - VC6,                            ::
::    8 - VC8 (2005),                     ::
::    9 - VC9,                            ::
::   10 - VC10 (2010)                     ::
::   11 - VC11 (2012)                     ::
:: -------------------------------------- ::
set iCompVer=11


:: -------------------------------------- ::
:: Режим компиляции:                      ::
::    0 - release,                        ::
::    1 - debug,                          ::
::    2 - генерирует прототипы функций    ::
::        (без компиляции)                ::
:: -------------------------------------- ::
set iModeComp=1

:: -------------------------------------- ::
::  Флаги наличия библиотеки:             ::
::      0 - нет, 1 - есть                 ::
:: -------------------------------------- ::
set bLibQt=0
set bLibCrt=0
set bLibBoost=0

:: -------------------------------------- ::
:: Режим линковки с Qt:                   ::
::    0 - shared (динамическая),          ::
::    1 - static (статическая)            ::
:: -------------------------------------- ::
set iModeQt=0

:: -------------------------------------- ::
:: Флаги специальных WINDDK               ::
::    0 - не используется                 ::
::    1 - WINDDK для Win2000, WinXP       ::
::        (в этом случае д.б. верно заданы::
::         gavIncPathDdkXP,gavLibPathDdkXP::
::         - см. ниже)                    ::
:: -------------------------------------- ::
set iWinddk=0

:: -------------------------------------- ::
::    Дополнительные флаги компилятора    ::
::   ( дефайны задавать так: -Dдефайн )   ::
:: -------------------------------------- ::
::set gavCompilFlags=-Dtest


:: -------------------------------------- ::
::     Подсистема приложения (одна из)    ::
:: -------------------------------------- ::
:: Win32 character-mode application: 
set gavSubsystem=CONSOLE
:: Application does not require a console:
::set gavSubsystem=WINDOWS
:: Device drivers for Windows NT:
::set gavSubsystem=NATIVE
:: Application that runs with the POSIX subsystem in Windows NT:
::set gavSubsystem=POSIX
:: Application that runs on a Windows CE device:
::set gavSubsystem=WINDOWSCE



:: -------------------------------------- ::
::          Пути к Visual Studio.         ::
::          !!! без кавычек !!!           ::
:: -------------------------------------- ::
:: VS6 (!! Путь к компилятору VS6 д.б. коротким и без пробелов - иначе ошибка линковки)
if %iCompVer%==6 set sVSPath=C:\Progs\VC98
if %iCompVer%==6 set sVSPathName=%sVSPath%\bin\cl.exe

:: VS8 
if %iCompVer%==8 set sVSPath=%SYSTEMDRIVE%\Program Files\Microsoft Visual Studio 8\VC
if %iCompVer%==8 set sVSPathName=%sVSPath%\bin\cl.exe

:: VS9
if %iCompVer%==9 set sVSPath=%SYSTEMDRIVE%\Program Files\Microsoft Visual Studio 9.0\VC
if %iCompVer%==9 set sVSPathName=%sVSPath%\bin\cl.exe

:: VS10
if %iCompVer%==10 set sVSPath=%SYSTEMDRIVE%\Program Files\Microsoft Visual Studio 10.0\VC
if %iCompVer%==10 set sVSPathName=%sVSPath%\bin\cl.exe
                   :: доп.пути для VS2010:
if %iCompVer%==10 set gavIncPathMy=%SYSTEMDRIVE%\Program Files\Microsoft SDKs\Windows\v7.0A\Include
if %iCompVer%==10 set gavLibPathMy=%SYSTEMDRIVE%\Program Files\Microsoft SDKs\Windows\v7.0A\Lib

:: VS11
if %iCompVer%==11 set sVSPath=%SYSTEMDRIVE%\Program Files (x86)\Microsoft Visual Studio 11.0\VC
if %iCompVer%==11 set sVSPathName=%sVSPath%\bin\cl.exe
                   :: доп.пути для VS2010:
if %iCompVer%==11 set gavIncPathMy=%SYSTEMDRIVE%\Program Files (x86)\Windows Kits\8.0\Include\um
if %iCompVer%==11 set gavIncPathMy2=%SYSTEMDRIVE%\Program Files (x86)\Windows Kits\8.0\Include\Shared
if %iCompVer%==11 set gavLibPathMy=%SYSTEMDRIVE%\Program Files (x86)\Windows Kits\8.0\Include\Lib
if %iCompVer%==11 set gavLibPathMy2=%SYSTEMDRIVE%\Program Files (x86)\Windows Kits\8.0\Lib\win8\um\x86
::if %iCompVer%==11 set gavLibPathMy2=%SYSTEMDRIVE%\Program Files (x86)\Windows Kits\8.0\Lib\win8\um\x64

echo Compilator version: VC%iCompVer%
echo Compilator path: %sVSPathName%

:: -------------------------------------- ::
::              Пути к Boost              ::
:: -------------------------------------- ::
set BOOST_ROOT=C:\Progs\boost\boost_1_49_0


:: -------------------------------------- ::
::     Настройка путей к подключаемым     ::
::  файлам.   Каждый путь должен быть     ::
::         отдельной переменной !         ::
:: -------------------------------------- ::
set gavIncPathVS="%sVSPath%\include"
set gavIncPathQt="%QTDIR%\include"
set gavIncPathBoost="%BOOST_ROOT%"
set gavIncPathDdkCrt="C:\Progs\WINDDK\2600.1106\inc\crt"
set gavIncPathDdkXP="C:\Progs\WINDDK\2600.1106\inc\w2k"

if not "%gavIncPathMy%"=="" (set gavIncPathAll=-I"%gavIncPathMy%" 
                             if not "%gavIncPathMy2%"=="" (
                                                           set gavIncPathAll=-I"%gavIncPathMy%" -I"%gavIncPathMy2%"
                                                          )
                            )
if %iWinddk%==1 (set gavIncPathAll=%gavIncPathAll% -I%gavIncPathDdkCrt% -I%gavIncPathDdkXP%)
if not %gavIncPathVS%=="" (set gavIncPathAll=%gavIncPathAll% -I%gavIncPathVS%)
if %bLibQt%==1 (if not %gavIncPathQt%=="" (set gavIncPathAll=%gavIncPathAll% -I%gavIncPathQt%))
if %bLibBoost%==1 (if not %gavIncPathBoost%=="" (set gavIncPathAll=%gavIncPathAll% -I%gavIncPathBoost%))
echo Include pathes: %gavIncPathAll%


:: -------------------------------------- ::
::  Настройка путей к библиотечным (.lib) ::
::  файлам.   Каждый путь должен быть     ::
::         отдельной переменной !         ::
:: -------------------------------------- ::
set gavLibPathDdkXP=C:\Progs\WINDDK\2600.1106\lib\wxp\i386
set gavLibPathVS=%sVSPath%\lib

set gavLibPathAll=""
if %iWinddk%==1 (set gavLibPathAll=%gavLibPathAll% /LIBPATH:"%gavLibPathDdkXP%")
if not "%gavLibPathVS%"=="" (set gavLibPathAll=%gavLibPathAll% /LIBPATH:"%gavLibPathVS%")
if not "%gavLibPathMy%"=="" (set gavLibPathAll=%gavLibPathAll% /LIBPATH:"%gavLibPathMy%"
                             if not "%gavLibPathMy2%"=="" (
                                                           set gavLibPathAll=%gavLibPathAll% /LIBPATH:"%gavLibPathMy%" /LIBPATH:"%gavLibPathMy2%"
                                                          )
                            )

set gavLibPathBoost="%BOOST_ROOT%\stage\lib"
if %bLibBoost%==1 (if not %gavLibPathBoost%=="" (set gavLibPathAll=%gavLibPathAll% /LIBPATH:%gavLibPathBoost%))

set gavLibPathQtReleaseShared="%QTDIR%\lib\release\shared"
set gavLibPathQtReleaseStatic="%QTDIR%\lib\release\static"
set gavLibPathQtDebugShared="%QTDIR%\lib\debug\shared"
set gavLibPathQtDebugStatic="%QTDIR%\lib\debug\static"
if %bLibQt%==1 (
                 if %iModeComp%==0 (
                                    if %iModeQt%==0 (
                                                     echo Qt mode: release shared
                                                     set gavLibPathAll=%gavLibPathAll% /LIBPATH:%gavLibPathQtReleaseShared%
                                                    )
                                    if %iModeQt%==1 (
                                                     echo Qt mode: release static
                                                     set gavLibPathAll=%gavLibPathAll% /LIBPATH:%gavLibPathQtReleaseStatic%
                                                    )
                                   )
                 if %iModeComp%==1 (
                                    if %iModeQt%==0 (
                                                     echo Qt mode: debug shared
                                                     set gavLibPathAll=%gavLibPathAll% /LIBPATH:%gavLibPathQtDebugShared%
                                                    )
                                    if %iModeQt%==1 (
                                                     echo Qt mode: debug static
                                                     set gavLibPathAll=%gavLibPathAll% /LIBPATH:%gavLibPathQtDebugStatic%
                                                    )
                                   )
               )
echo Lib pathes: %gavLibPathAll%

:: -------------------------------------- ::
::      Файлы библиотеки run-time.        ::
:: При необходимости добавить сюда нужные ::
:: -------------------------------------- ::
set gavLibFilesCrt=""
if %bLibCrt%==1 (set gavLibFilesCrt=user32.lib ole32.lib Gdi32.lib Ws2_32.lib Imm32.lib Comdlg32.lib Winspool.lib Advapi32.lib)
if not "%gavLibFilesCrt%"=="" (set gavLibFilesAll=%gavLibFilesCrt%)

:: -------------------------------------- ::
::         Файлы библиотеки Qt.           ::
:: -------------------------------------- ::
set gavLibFilesQtShared=qtmain.lib qt-mt333.lib
set gavLibFilesQtStatic=qtmain.lib qt-mt.lib

if %bLibQt%==1 (
                if %iModeQt%==0 (set gavLibFilesAll=%gavLibFilesAll% %gavLibFilesQtShared%)
                if %iModeQt%==1 (set gavLibFilesAll=%gavLibFilesAll% %gavLibFilesQtStatic%)
               )

echo Lib files: %gavLibFilesAll%


:: -------------------------------------- ::
::     Настройка режимов компиляции       ::
:: -------------------------------------- ::
if %iModeComp%==0 (
               set gavLinkMode=/RELEASE
               :: для DLL: set gavCompMode=/MD
               set gavCompMode=/MT
               set gavOptimize=/O2 /GA
              )

if %iModeComp%==1 (
               set gavLinkMode=/DEBUG
               :: для DLL: set gavCompMode=/MDd
               set gavCompMode=/MTd
               ::set gavOptimize=/Od /ZI
               set gavOptimize=/Od /Z7
              ) 

if %iModeComp%==2 (
               set gavLinkMode=
               set gavCompMode=
               set gavOptimize=/Zg
              ) 

if %bLibQt%==1 (if %iModeQt%==1 (set gavCompMode=/MDd))

:: -------------------------------------- ::
::    Настройка подсистемы приложения     ::
:: -------------------------------------- ::
if not %gavSubsystem%=="" (set gavLinkSubsys=/SUBSYSTEM:%gavSubsystem%)


:: -------------------------------------- ::
::         Настройка компилятора          ::
:: -------------------------------------- ::
if %bLibQt%==1 (
                if %iModeQt%==0 (set gavCompilFlags=%gavCompilFlags% -DQT_DLL)
               )

:: -------------------------------------- ::
::        Удаление старых файлов          ::
:: -------------------------------------- ::
set gavOldObj=%gavSrc:.cpp=.obj,%
set gavOldObj=%gavOldObj:.c=.obj,%
set gavOldAsm=%gavOldObj:.obj=.asm%
for /F "tokens=1" %%A in ("%gavSrc%") do (set gavMainName=%%A)
set gavMainName=%gavMainName:.cpp=%
set gavMainName=%gavMainName:.c=%
set gavDelExt= %gavMainName%.exe, %gavMainName%.pdb, %gavMainName%.ilk, %gavOldObj% %gavOldAsm% __vc++_%gavMainName%.log
echo. 
echo Deleting old files: %gavDelExt% ...
echo. 
del %gavDelExt%
echo. 
echo ------------------
echo Compiling start...
echo ------------------
echo. 

if %bLibQt%==1 (if not %gavSrcQtMoc%=="" (
echo ------------------
echo Mocing file... 
echo ------------------
echo on
%QTDIR%\bin\moc %gavSrcQtMoc% -o moc_%gavSrcQtMoc%.cpp
@echo off
set gavSrc=%gavSrc% moc_%gavSrcQtMoc%.cpp
))

echo on
"%sVSPathName%" /EHsc %gavIncPathAll% %gavCompilFlags% /Fa %gavSrc% %gavCompMode% %gavOptimize% /link %gavLinkSubsys% %gavLinkMode% %gavLibPathAll% %gavLibFilesAll%>"__vc++_%gavMainName%.log"

@echo off
echo. 
echo ------------------
echo Compiling end...
echo ------------------
echo. 
echo -------------------------------------- 
echo ------ %date% [%time%] ------
echo -------------------------------------- 
echo ---------- .:: -=LEXX=- ::. ----------
echo -------------------------------------- 
:: -------------------------------------- ::
pause